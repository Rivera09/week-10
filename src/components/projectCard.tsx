import React from "react"
import { Link } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image"

const projectCard = ({ slug, title, tags, image }) => {
  return (
    <div className="project-container">
      <Link to={`/${slug}`}>
        <GatsbyImage image={image} alt={title} />
        <h3 className="fw-regular">{title}</h3>
        <div className="tech-tags">
          {tags.map((tag, i: number) => (
            <p key={"tag" + i}>{tag}</p>
          ))}
        </div>
      </Link>
    </div>
  )
}

export default projectCard
