import React from "react"

const technologyStack = ({ title, icon, children }) => {
  return (
    <div className="technology">
      {icon}
      <h3>{title}</h3>
      <ul>{children}</ul>
    </div>
  )
}

export default technologyStack
