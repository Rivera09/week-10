import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

const About = () => (
  <Layout>
    <Seo title="About" />
    <section className="about">
      <h2 className="title fw-regular">About me</h2>
      <div className="about-container">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero,
          alias iste, soluta adipisci deleniti rerum est illum inventore qui
          iure error quaerat fuga voluptatem quae eius ab! Ratione, aperiam.
          Quibusdam minus, vero impedit architecto ut possimus alias dicta,
          laboriosam earum facere, sit nobis blanditiis. Animi vero modi
          quisquam soluta voluptatum iure asperiores molestias dolor. Porro
          beatae assumenda impedit, culpa temporibus, eveniet veritatis illo,
          repudiandae ducimus numquam facere? Qui veniam expedita labore
          facilis, iure necessitatibus eum aliquam doloremque neque est amet
          nulla quod rerum cum? Consequuntur nesciunt dolorem perspiciatis
          facere quam repellendus aperiam eligendi vero, temporibus minus
          voluptatem cupiditate tempora doloremque!
        </p>
      </div>
    </section>
  </Layout>
)

export default About
