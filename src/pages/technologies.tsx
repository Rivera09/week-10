import React from "react"
import Carousel from "react-elastic-carousel"

import { ITag, ITagCategory } from "../types"
import SEO from "../components/seo"
import projectsData from "../../projects.json"
import Layout from "../components/layout"
import Stack from "../components/technologyStack"

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2, itemsToScroll: 2 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 },
]

const techologies = () => {
  const category1: ITag[] = []
  const category2: ITag[] = []
  const category3: ITag[] = []
  const tagCategories: ITagCategory[] = projectsData.tagCategories

  projectsData.tags.forEach(tech => {
    switch (tech.category) {
      case 1:
        category1.push(tech)
        break
      case 2:
        category2.push(tech)
        break
      default:
        category3: category3.push(tech)
    }
  })

  return (
    <Layout>
      <SEO title="Technologies" />
      <section className="technologies">
        <h2 className="title fw-regular">Technologies</h2>
        <p className="subtitle">Technologies I have experience with</p>
        <Carousel
          breakPoints={breakPoints}
          className="center-div"
          showEmptySlots={false}
        >
          <Stack
            title={tagCategories[0].name}
            icon={<i className="fas fa-laptop-code"></i>}
          >
            {category1.map(tech => (
              <li key={tech.id}>{tech.name}</li>
            ))}
          </Stack>
          <Stack
            icon={<i className="fas fa-server"></i>}
            title={tagCategories[1].name}
          >
            {category2.map(tech => (
              <li key={tech.id}>{tech.name}</li>
            ))}
          </Stack>
          <Stack
            icon={<i className="fas fa-toolbox"></i>}
            title={tagCategories[2].name}
          >
            {category3.map(tech => (
              <li key={tech.id}>{tech.name}</li>
            ))}
          </Stack>
        </Carousel>
        {/* <div className="technologies-flexbox "> */}

        {/* </div> */}
      </section>
    </Layout>
  )
}

export default techologies
