exports.createPages = async ({ actions }) => {
  const projectsJSON = require("./projects.json")
  const projects = projectsJSON.projects
  projects.forEach(project => {
    let layout

    switch (project.layout) {
      case "layout 1":
        layout = "./src/components/layout1.tsx"
        break
      case "layout 2":
        layout = "./src/components/layout2.tsx"
        break
      default:
        layout = "./src/components/layout1.tsx"
    }

    actions.createPage({
      path: project.slug,
      component: require.resolve(layout),
      context: {
        project,
        directory: `/projects/${project.title.replace(" ", "-")}$/`,
      },
    })
  })
}
