import React from "react"

const contactSquare = ({ title, icon, children }) => {
  return (
    <div className="contact-square">
      {icon}
      <h3>{title}</h3>
      {children}
    </div>
  )
}

export default contactSquare
