import React, { useState } from "react"
import { Link } from "gatsby"

const Header = () => {
  const [active, setActive] = useState<boolean>(false)

  return (
    <nav className="navigation-menu">
      <ul className={`${active ? "active" : ""}`}>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/technologies">Technologies</Link>
        </li>
        <li>
          <Link to="/projects">Projects</Link>
        </li>
        <li>
          <Link to="/contact">Contact</Link>
        </li>
      </ul>
      <div
        className={`burger ${active ? "active" : ""}`}
        onClick={() => setActive(prevValue => !prevValue)}
      >
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
    </nav>
  )
}

export default Header
