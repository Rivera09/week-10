import React, { useMemo } from "react"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { Carousel } from "react-responsive-carousel"
import "react-responsive-carousel/lib/styles/carousel.min.css" // requires a loader

import Layout from "./layout"
import projectsJSON from "../../projects.json"

const allTags = projectsJSON.tags

export const query = graphql`
  query($directory: String!) {
    images: allFile(
      filter: {
        sourceInstanceName: { eq: "images" }
        relativeDirectory: { regex: $directory }
      }
    ) {
      nodes {
        sharp: childImageSharp {
          gatsbyImageData
        }
      }
    }
  }
`

const projectDetails = ({ pageContext, data }) => {
  const project = useMemo(() => pageContext.project, [])
  const { description, title } = project
  let imagesSharps = data.images.nodes.map(node => node.sharp)
  imagesSharps = imagesSharps.filter(sharp => (sharp ? true : false))

  const tags = allTags.filter(tag => project.stack.includes(tag.id))

  return (
    <Layout>
      <div className="layout1-container">
        <h2 className="title">{title}</h2>
        <p>{description}</p>
        <h3>Development stack</h3>
        <ul className="stack-list">
          {tags.map(tag => (
            <li key={tag.id}>{tag.name}</li>
          ))}
        </ul>
        <Carousel showThumbs={false}>
          {imagesSharps.map((sharp, i) => {
            const image = getImage(sharp)
            return <GatsbyImage image={image} alt="screenshot" key={i} />
          })}
        </Carousel>
      </div>
    </Layout>
  )
}

export default projectDetails
