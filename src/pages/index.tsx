import * as React from "react"
// import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import Typewriter from "typewriter-effect"

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <section className="intro">
      <div className="intro-box">
        <div className="intro-box-flex">
          <StaticImage
            src="../images/jorge-rivera.jpg"
            width={300}
            quality={95}
            formats={["AUTO", "WEBP", "AVIF"]}
            alt="Jorge Rivera"
            // style={{ marginBottom: `1.45rem` }}
          />
          {/* <img src="../images/jorger-rivera.jpg" alt="jorge rivera" /> */}
          <div className="intro-text">
            <h1 className="fw-regular">
              Hi, I'm <br />
              <strong className="fw-strong">Jorge Rivera</strong>
            </h1>
            <div className="typing-text-box fw-300">
              <Typewriter
                // options={{
                //   strings: [
                //     "I am a front-end developer",
                //     "I am a back-end developer",
                //     "I am a developer",
                //   ],
                //   autoStart: true,
                //   delay: 5,
                //   deleteSpeed: 30,
                //   loop: true,
                // }}
                onInit={typewriter => {
                  typewriter
                    .changeDelay(50)
                    .changeDeleteSpeed(5)
                    .typeString("I'm a front-end developer")
                    .pauseFor(1500)
                    .deleteChars(19)
                    .typeString("back-end developer")
                    .pauseFor(1500)
                    .deleteChars(18)
                    .typeString("developer")
                    .start()
                }}
              />
            </div>
          </div>
        </div>
        <div className="contact-box">
          <p className="tel-info">tel:+504 9557221</p>
          <p className="email-info">email:jorge-421@hotmail.com</p>
        </div>
      </div>
      <div className="social-networks-btns">
        <a
          className="github-btn link-btn"
          href="https://github.com/Rivera09"
          target="_blank"
        >
          <i className="fab fa-github"></i>
        </a>
        <a
          className="linkedin-btn link-btn"
          href="https://www.linkedin.com/in/jorge-rivera-165231169/"
          target="_blank"
        >
          <i className="fab fa-linkedin-in"></i>
        </a>
      </div>
    </section>
  </Layout>
)

export default IndexPage
