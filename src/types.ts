export interface ITag {
  id: number
  name: string
  category: number
}

export interface ITagCategory {
  id: number
  name: string
}

export interface IProjectsJSON {
  tags: ITag[]
  tagCategories: ITagCategory[]
}
