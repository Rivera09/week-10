import React from "react"
import Carousel from "react-elastic-carousel"

import Layout from "../components/layout"
import Square from "../components/contactSquare"

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2, itemsToScroll: 2 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 },
]

const contact = () => {
  return (
    <Layout>
      <section className="contact">
        <h2 className="title fw-regular">Contact</h2>

        <Carousel
          breakPoints={breakPoints}
          className="center-div"
          showEmptySlots={false}
        >
          <Square title="Call me" icon={<i className="fas fa-mobile-alt"></i>}>
            <p>+504 95117221</p>
          </Square>
          <Square title="Email me" icon={<i className="fas fa-at"></i>}>
            <p>jorge-421@hotmail.com</p>
          </Square>
          <Square
            title="Add me on Linkedin"
            icon={<i className="fab fa-linkedin-in"></i>}
          >
            <a href="https://www.linkedin.com/in/jorge-rivera-165231169/">
              Visit
            </a>
          </Square>
          <Square
            title="Checkout my projects"
            icon={<i className="fab fa-github"></i>}
          >
            <a href="https://github.com/Rivera09">Visit</a>
          </Square>
        </Carousel>
      </section>
    </Layout>
  )
}

export default contact
