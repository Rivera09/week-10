import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import { getImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import projectsJSON from "../../projects.json"
import ProjectCard from "../components/projectCard"
import Pagination from "../components/pagination"

const projectsList = projectsJSON.projects
const projectsTags = projectsJSON.tags

export const query = graphql`
  query {
    images: allFile(
      filter: {
        sourceInstanceName: { eq: "images" }
        name: { regex: "/img0/" }
      }
    ) {
      nodes {
        relativePath
        sharp: childImageSharp {
          gatsbyImageData
        }
      }
    }
  }
`

const projects = ({ data }) => {
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [displayedProjects, setDisplayedProjects] = useState([])
  const [selectedTags, setSelectedTags] = useState<number[]>([])
  const [total, setTotal] = useState<number>(0)

  const selectTag = (id: number) => {
    const tempTags = selectedTags.includes(id)
      ? selectedTags.filter(tagId => tagId !== id)
      : [...selectedTags, id]
    setSelectedTags(tempTags)
    if (tempTags.length) {
      const temp = projectsList.filter(project =>
        project.stack.some(stackId => tempTags.includes(stackId))
      )
      setDisplayedProjects(temp.slice(0, 8))
      setTotal(temp.length)
    } else {
      setDisplayedProjects(projectsList.slice(0, 8))
      setTotal(projectsList.length)
    }
    setCurrentPage(1)
  }

  const changePage = (index: number) => {
    setCurrentPage(index)
  }

  useEffect(() => {
    if (!selectedTags.length) {
      setDisplayedProjects(
        projectsList.slice(currentPage * 8 - 8, currentPage * 8)
      )
    } else {
      const temp = projectsList.filter(project =>
        project.stack.some(stackId => selectedTags.includes(stackId))
      )
      setDisplayedProjects(temp.slice(currentPage * 8 - 8, currentPage * 8))
    }
    if (!total) setTotal(projectsList.length)
  }, [currentPage])

  const images = data.images.nodes.map(node => ({
    title: node.relativePath.replace("projects/", "").replace("/img0.png", ""),
    image: getImage(node.sharp),
  }))

  return (
    <Layout>
      <section className="work">
        <h2 className="title fw-regular">My work</h2>
        <p className="subtitle">Some projects I've worked on</p>
        <div className="projects-tags">
          {projectsTags.map(tag => (
            <p
              key={tag.id}
              className={`${selectedTags.includes(tag.id) ? "active" : ""}`}
              onClick={() => selectTag(tag.id)}
            >
              {tag.name}
            </p>
          ))}
        </div>
        <div className="work-container ">
          {displayedProjects.map(project => {
            const tags = projectsTags.filter(tag =>
              project.stack.includes(tag.id)
            )
            const imageData = images
              .filter(item => item.title === project.title.replace(" ", "-"))
              .pop()
            return (
              <ProjectCard
                title={project.title}
                slug={project.slug}
                key={project.id}
                tags={tags.map(tag => tag.name)}
                image={imageData?.image}
              />
            )
          })}
        </div>
        <Pagination
          total={total}
          changePage={changePage}
          currentPage={currentPage}
          amountPerPage={8}
        />
      </section>
    </Layout>
  )
}

export default projects
