import React from "react"

const slick = ({ symbol, onClick, active, disabled }) => {
  return (
    <div
      className={`slick ${active ? "active" : ""} ${
        disabled ? "disabled" : ""
      }`}
      onClick={() => {
        !disabled ? onClick() : null
      }}
    >
      {symbol}
    </div>
  )
}

export default slick
