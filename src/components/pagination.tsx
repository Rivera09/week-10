import React, { useMemo } from "react"

import Slick from "../components/slick"

const pagination = ({ total, changePage, currentPage, amountPerPage }) => {
  // console.log({ total })
  // console.log({ amountPerPage })
  const totalPages = useMemo(
    () => Array(Math.ceil((total) / amountPerPage)).fill(""),
    [total, amountPerPage]
  )
  return (
    <div className="pagination">
      <Slick
        symbol="<"
        onClick={() => changePage(currentPage - 1)}
        active={false}
        disabled={currentPage - 1 === 0}
      />

      {totalPages.map((index, i) => (
        <Slick
          symbol={i + 1}
          key={`slick-${i + 2}`}
          onClick={() => changePage(i + 1)}
          active={currentPage === i + 1}
          disabled={false}
        />
      ))}
      <Slick
        symbol=">"
        onClick={() => changePage(currentPage + 1)}
        active={false}
        disabled={currentPage === totalPages.length}
      />
    </div>
  )
}

export default pagination
