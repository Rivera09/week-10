import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="404: Not found" />
    <div className="not-found">
      <h1>404</h1>
      <p>
        Whatever you were looking for, it isn't here, but you just found the 404
        lucky page. This page appears once every 10,000 pages. Now make a wish
        and then go to the <Link to="/">home page</Link>
      </p>
      <StaticImage src="../images/pepe.png" alt="lucky pepe" />
    </div>
  </Layout>
)

export default NotFoundPage
